#!/usr/bin/python3
# coding: utf-8

"""
Replace markups with string in a text file,
handy thing if you want to use user defined
emojis or icons that are recurrent to type.

Author: Thierry Vala

Created: 25 nov 2021
"""

from string import Template
import sys
import os

def extractconfig(s_data, delimiter=": "):
    """
    extract config from string
    
    Parameters
    ---
    s_data : str
        input text that contains config, aka markup string to be replaced by text
    delimiter : str
        delimiter between markup keyword and string to be replace with

    Return
    ---
    d_config : dict
        dictionnary with keywords that leads to the string to be replaced with
    """
    t_data = s_data.split("\n")
    d_config = {}
    for s_e in t_data:
        if delimiter in s_e : # if we got a parameter on this line
            s_key,s_value = s_e.split(delimiter,1)
            s_key = s_key.strip()
            s_value = s_value.strip()
            d_config[s_key]=s_value
    return d_config

class TemplateAssets(Template):
    """
    New class template to match ":" delimiter
    Same as Template but match :var: instead of ${var}
    :keyword: can now be used in a string to be replace by something
    """
    delimiter = ':'
    pattern = r'''
    :(?:
    (?P<escaped>:)|
    (?P<named>[_a-z][_a-z0-9]*):|
    (?P<braced>[_a-z][_a-z0-9]*):|
    (?P<invalid>::)
    )
    '''

def useassets(s_template,d_config):
    """
    Apply template. Replace markup by it entry in the dictionnary

    Parameters
    ---
    s_template : str
        the template containing the markups
    d_config : dict
        the dictionnary that contains the entry related to the markup in the template

    Return
    ---
    s_result: str
        the template with the markups replaced
    """
    template = TemplateAssets(s_template)
    s_result = template.substitute(d_config)
    return s_result

def apply_on_file(input_file_name, output_file_name, config_file_name):
    """
    Make a new file with markup replaced from config in config_file.

    Parameters
    ---
    input_file_name: str
        input file name
    output_file_name: str
        output file name
    config_file_name: str
        config file name 
    """
    
    # loads the files
    with open(input_file_name,"r") as f:
        s_in = f.read()
    with open(config_file_name, "r") as f:
        s_conf = f.read()

    # get the conf from the conf file
    d_conf = extractconfig(s_conf)

    # apply template
    s_out = useassets(s_in, d_conf)

    # write new file
    with open(output_file_name,"w+") as f:
        f.write(s_out)


# when use it as a script
if __name__ == '__main__' :
    if len(sys.argv) == 4:
        print(" ".join(sys.argv))
        current = os.getcwd()
        fin = os.path.join(current, sys.argv[1])
        fout = os.path.join(current, sys.argv[2])
        fconf = os.path.join(current, sys.argv[3])
        print(fin)
        print(fout)
        print(fconf)
        apply_on_file(fin, fout, fconf)
    else:
        print(" ".join(sys.argv))
        print(f"Wrong number of arguments, received {len(sys.argv)-1}, expect 3.")