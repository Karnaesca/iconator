# Iconator

Just a quick templaty thingy that replace text based markup with other define strings. Just abusing the native template thing of python.

## Usage

```python
import sys
sys.path.append("iconator/") #specify the path of the iconator library 

import iconator # import the library

iconator.apply_on_file("input_file.txt", "output_file.txt", "config.conf")
```

*input_file.txt:*
```
keyword
:keyword:
::keyword:
:keyword::
::keyword::

bla:keyword:bla
bla::keyword:bla
bla:keyword::bla
bla::keyword::bla
```

*config.conf:*
```
keyword : coucou
```

as a result, *output_file.txt*:
```
keyword
coucou
:keyword:
coucou:
:keyword:

blacoucoubla
bla:keyword:bla
blacoucou:bla
bla:keyword:bla
```

